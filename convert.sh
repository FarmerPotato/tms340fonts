#!/bin/bash

# Convert all fonts in FNT/ to BMP/
# Print report on stdout
# For other bitmap conversions, get imagemagick (bmp to png for example)

indir=FONTS
outdir=BMP

for file in $indir/*.FNT 
do

font=`basename -s .FNT $file`
./fnt2bmp $file $outdir/$font.BMP -

done

