

	   ****************************************************
	   *						      *
	   *		   Font File Conversion 	      *
	   *   Between TIGA and the TMS340 Graphics Library   *
	   *						      *
	   ****************************************************


The bit-mapped fonts distributed with the TMS340 Graphics Library
reside in the \fonts directory.  These fonts are distributed in the file
format required for use with the graphics library's text functions.
However, you may have occasion in the future to convert graphics library
fonts to and from the file format required for use with TIGA.  This
readme file describes the conversion process in both directions.

The font archive files in this directory are identified by their *.lib
extensions.  Each archive file contains all the fonts in a particular
typeface.  The archive is built using the gspar.exe archive utility
program described in the TMS340 Family Code Generation Tools User's
Guide.

As described in Chapter 1 of the TMS340 Graphics Library User's Guide,
these fonts are identical to those distributed with the TIGA Software
Interface package, although the font file format differs from that of
TIGA.  Each TIGA font is distributed as a binary image file with a *.fnt
file name extension.  Each font in the TMS340 Graphics Library is
distributed as a file in COFF format with a *.obj file name extension.
The conversion between the TIGA and graphics library formats is
straightforward using the binsrc.exe and cof2bin.exe conversion utilities
that are distributed with the library, and reside in the \fonts
subdirectory.  These utilities are described in the TMS340 Software
Developer's Kit User's Guide.

For example, to convert the Austin size 25 font from graphics library
format to TIGA format, the austin25.obj file is first extracted by the
gspar.exe utility from the austin.lib font archive with the following
MS-DOS command:

		     gspar -x austin.lib austin25.obj

Using the cof2bin.exe conversion utility, the MS-DOS command to convert
the austin25.obj file, in COFF format, to binary format is

		     cof2bin austin25.obj austin25.fnt

where the output file, austin25.fnt, is in the binary format used by
TIGA.

Continuing the example, the TIGA font file austin25.fnt is converted to
the graphics library format in two steps.  First, the binsrc.exe
conversion utility is used to convert the TIGA font file to an TMS340
Assembly Language source file with the following MS-DOS command:

		   binsrc -a austin25.fnt austin25.asm

If you inspect the resulting source file, austin25.asm, you will notice
that the label assigned to the font data structure is "austin25", which
is the file name as well.  By convention, the file name (and the label)
matches the global name assigned to this font within the graphics
library.  The conversion from assembly source to COFF format is done by
invoking the TMS340 Assembler in the usual manner:

		   gspa austin25.asm austin25.obj

The resulting output file, austin25.obj, is in the COFF format used for
the fonts distributed with the graphics library.

----end of file----
