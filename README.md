# tms340fonts

TI commissioned fonts for the 34010 graphics processor (TIGA cards).
Utility converts the FNT files to BMP. Handles both formats.
Work in progress.

make

fnt2bmp FONTS/ BMP/

or do them all with:

./convert.sh

FONTS/

All the FNT files from the 1990 SDK.

FONTS_LIB/

The original TMS34010 Font Library disk. Distributed separately from the 1987 SDK.
(I found the 1987 Font Library User's Guide in the TI archive at SMU. That's what
motivated me to investigate this project!)

The LIB files are in the TI compiler archive format. See README.1st
You can still peek inside with modern(!) ar. But you need a DOS emulator to 
do their conversion of obj->src.

Would be a good test of the 1987 format. Content is redundant with FONTS.

