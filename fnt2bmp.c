/**********************************************************************************

	fnt2bmp.c - Translate TMS340 gdk fonts into other formats.
	
           Usage: fnt2bmp inputfile outputfile [reportfile]
           
           inputfile:  x.FNT
           outputfile: x.BMP
           reportfile: details of translation
                      
 (c)2021 Erik Olson. No restrictions on use. See LICENSE.

**********************************************************************************/

#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/errno.h>
#include <sys/stat.h>

void process_file(FILE* ifp, FILE* ofp, FILE* rfp, const char* input_filename);
unsigned char* read_file(FILE *ifp, size_t len);
void process_binary(unsigned char* buf, int len, FILE* ofp, FILE* rfp);

int  take_bits(unsigned char** ptr, int n, int *pbit);


/**
*   There are two formats of FNT file.
* 	fonttype/magic is the first word in the header.
*
*   Fonts from the latest known compiler disk have magic# 8040.
*   There are some in the older format, with fonttype 9000.
*   This utility aims to handle each of them.
**/


/**

	1987 TMS34010 Math/Graphics Function Library User's Guide

The font library is a separate product that contains a variety of proportionally spaced and monospaced fonts.

The library supports both proportionally spaced and monospaced fonts, and includes several fonts. Additional fonts are available with the TMS34010 Font Library (see the TMS34010 Font Library User's Guide for more information).

		fonttype 
		
		Font type code. This code designates the type of font. Proportionally 
		spaced fonts are identified by a fonttype value of 9000h. (The software treats 
		even monospaced fonts such as Corpus-Christi as proportionally spaced fonts.)

	1990 TMS340 Graphics Library User’s Guide
	
		magic
		
		This field contains the value 0x8040, a code that designates the FONT structure
		for bit-mapped fonts above. If alternate data structures for stroke or outline 
		fonts are supported in the future, these will be distinguished by alternate 
		magic codes.

	Using magic=8.4 in the 1990 version is confusing, to say the least.

	What point size does the pixel height correspond to?	
**/


/*-------------------------------------------------------------- 
 *  TMS34010 Graphics Function Library
 *--------------------------------------------------~-----------
 * font_struct data structure
 *
 * Data structure for storage of text font bit map and attributes. 
 * The charpatn , loctable , and rowtable arrays vary in size 
 * according to the font. This is the reason they are given dummy 
 * declarations below. The routines that manipulate these arrays
 * are written in assembly code.
 *--------------------------------------------------------------
 */
 
const uint16_t MAGIC84 = 0x8040;   // 1990 book
const uint16_t MAGIC90 = 0x9000;   // 1987 book

#pragma pack(2)
typedef struct {
	uint16_t magic;     /* font type code 9000 for 1987 */
	int16_t  firstchar;  /* ASCII code of first character */ 
	int16_t  lastchar;   /* ASCII code of last character */
	int16_t  widemax;    /* maximum character width */
	int16_t  kernmax;    /* maximum character kerning amount */ 
	int16_t  ndescent;   /* negative of the descent value */
	int16_t  frectwide;  /* width of font rectangle */ 
	int16_t  charhigh;   /* character height */ 
	int16_t  owtloc;     /* offset to offset/width table */
	int16_t  ascent;     /* ascent (how far above baseline) */
	int16_t  descent;    /* descent (how far below baseline) */ 
	int16_t  leading;    /* leading (row bottom to next row top) */ 
	int16_t  rowwords;   /* no. of words per row of char patterns */
	/* uint16_t charpatn[n]; character pattern bitmap */
	/* uint16_t loctable[n]; character offsets in charpatn */
	/* uint16_t owtable[n];  offset/width table */ 
	
} FONT90;

 
typedef struct {
	uint16_t magic;     /* aka fonttype. font type code 8040 for 1990 */
	uint32_t length;    /* length of file */
	char	   facename[30];  /* ASCII string name of font */
	int16_t  deflt;      /* default for missing character */
	int16_t  first;      /* first ASCII code in font */ 
	int16_t  last;       /* last ASCII code in font */
	int16_t  maxwide;    /* maximum character width */
	int16_t  maxkern;    /* maximum character kerning amount */ 
	int16_t  charwide;  /* block font character width */ 
	int16_t  avgwide;  /* average character width */ 
	int16_t  charhigh;   /* character height */ 
	int16_t  ascent;     /* ascent of highest character */
	int16_t  descent;    /* longest descender  */ 
	int16_t  leading;    /* separation between text rows */ 
	uint32_t rowpitch;   /* bit pitch of pattern table, multiple of 16 */
	uint32_t oPatnTbl;   /* offset to pattern table, even multiple of 16 */
	uint32_t oLocTbl;    /* offset to location table, even multiple of 16 */
	uint32_t oOwTbl;     /* offset to offset/width table, even multiple of 16 */ 
	
} FONT84;
#pragma options align=reset

typedef union {
	uint16_t magic;
	FONT84   font84;
  FONT90   font90;
} FONT;


/* common info */
typedef struct {
	int16_t   deflt;      /* default for missing character */
	int16_t   first;      /* first ASCII code in font */ 
	int16_t   last;       /* last ASCII code in font */
	int16_t   charhigh;   /* character height */ 
	int16_t   ascent;     /* ascent of highest character */
	int16_t   descent;    /* longest descender  */ 
	int16_t   leading;    /* separation between text rows */ 
	size_t    stride;     /* byte pitch of pattern table, multiple of 2 */
	int16_t   offset;     /*  */
	uint8_t*  patnTbl;   /* pattern table, even multiple of 16 */
	uint16_t* locTbl;    /* location table, even multiple of 16 */
	uint16_t* owTbl;     /* offset/width table, even multiple of 16 */ 
} FONTINFO;

// A canvas to draw on
typedef struct {
	int16_t width;  // in pixels
	int16_t height;
	int16_t stride; // in bytes, multiple of 4
	uint8_t *patnTbl;   // 8 pixels per byte, msb first
} BITMAP;


size_t bytes(uint32_t bits) {
	return (size_t)bits>>3;
}

/*-------------------------------------------------------------- */


void print_font_header_90(FONT90* font, FILE* rfp) {
	fprintf(rfp, "magic     %x\n", font->magic); // 9000 for 1987
	fprintf(rfp, "firstchar %d\n", font->firstchar );
	fprintf(rfp, "lastchar  %d\n", font->lastchar ); 
	fprintf(rfp, "widemax   %d\n", font->widemax ); 
	fprintf(rfp, "kernmax   %d\n", font->kernmax ); 
	fprintf(rfp, "ndescent  %d\n", font->ndescent ); 
	fprintf(rfp, "frectwide %d\n", font->frectwide ); 
	fprintf(rfp, "charhigh  %d\n", font->charhigh ); 
	fprintf(rfp, "owtloc    %d\n", font->owtloc ); 
	fprintf(rfp, "ascent    %d\n", font->ascent ); 
	fprintf(rfp, "descent   %d\n", font->descent ); 
	fprintf(rfp, "leading   %d\n", font->leading ); 
	fprintf(rfp, "rowwords  %d\n", font->rowwords );	
}


void print_font_header_84(FONT84* font, FILE* rfp) {
	fprintf(rfp, "magic     0x%x\n", font->magic);
	fprintf(rfp, "length    %d\n", (font->length));
	fprintf(rfp, "facename  %s\n", font->facename);
	fprintf(rfp, "deflt     %d\n", font->deflt );
	fprintf(rfp, "first     %d\n", font->first );
	fprintf(rfp, "last      %d\n", font->last ); 
	fprintf(rfp, "maxwide   %d\n", font->maxwide ); 
	fprintf(rfp, "maxkern   %d\n", font->maxkern ); 
	fprintf(rfp, "charwide  %d\n", font->charwide ); 
	fprintf(rfp, "avgwide   %d\n", font->avgwide ); 
	fprintf(rfp, "charhigh  %d\n", font->charhigh ); 
	fprintf(rfp, "ascent    %d\n", font->ascent ); 
	fprintf(rfp, "descent   %d\n", font->descent ); 
	fprintf(rfp, "leading   %d\n", font->leading ); 
	fprintf(rfp, "rowpitch  %d\n", font->rowpitch );
	fprintf(rfp, "oPatnTbl  %d\n", font->oPatnTbl );
	fprintf(rfp, "oLocTbl   %d\n", font->oLocTbl );
	fprintf(rfp, "oOwTbl    %d\n", font->oOwTbl );
}


void print_font_header(FONT* font, FILE* rfp) {
	uint16_t magic = font->magic;

	switch(magic) {
		case MAGIC90:
			print_font_header_90(&font->font90, rfp);
			break;
		case MAGIC84:
			print_font_header_84(&font->font84, rfp);
			break;
		default:
			fprintf(stderr, "Unknown font magic number 0x%x\n", magic);
			break;
	}
}

/**
	FONT* Accessors
**/

void get_font_info_90(FONT90* font, FONTINFO* info) {
	// tbd
	
}

void get_font_info_84(FONT84* font, FONTINFO* info) {
	uint8_t* raw    = (uint8_t*)font;

	info->deflt     = font->deflt;
	info->first     = font->first;
	info->last      = font->last; 
	info->charhigh  = font->charhigh; 
	info->ascent    = font->ascent; 
	info->descent   = font->descent; 
	info->leading   = font->leading; 
	info->stride    = font->rowpitch >> 4;
	info->patnTbl   = raw + (font->oPatnTbl >> 4);
	info->locTbl    = (uint16_t*)raw + (font->oLocTbl >> 4);
	info->owTbl     = (uint16_t*)raw + (font->oOwTbl >> 4);
}

void get_font_info(FONT* font, FONTINFO* info) {
	uint16_t magic = font->magic;

	switch(magic) {
		case MAGIC90:
			get_font_info_90(&font->font90, info);
			break;
		case MAGIC84:
			get_font_info_84(&font->font84, info);
			break;
		default:
			fprintf(stderr, "Unknown font magic number 0x%x\n", magic);
			break;
	}
}


/*
** BMP file structs 
*/

#pragma pack(2)
typedef struct  {
	uint8_t  id[2];
	uint32_t size;
	uint16_t reserved1, reserved2;
	uint32_t imageDataOffset;
} BITMAPFILEHEADER;

typedef struct  {
	uint32_t size;
	int32_t width;
	int32_t height;
	uint16_t numColorPlanes;
	uint16_t bpp;
	uint32_t compressionMethod;
	uint32_t imageSize;
	uint32_t horizontalResolution;
	uint32_t verticalResolution;
	uint32_t numColors;
	uint32_t numImportantColors;
} BITMAPINFOHEADER;

typedef struct {
	uint32_t black;
	uint32_t white;
} BITMAPCOLORTABLE;

#pragma options align=reset

/*
** BMP file routines 
*/


/* Padding to make imageData start on 4-byte boundary */
size_t paddingToImageData() {
	uint32_t imageDataOffset = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + sizeof(BITMAPCOLORTABLE);
	return imageDataOffset & 0x2;
}

void bmp_initialize_hdr(BITMAPINFOHEADER* dib, BITMAPFILEHEADER* hdr) {
	uint32_t imageDataOffset = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + sizeof(BITMAPCOLORTABLE);
	if (imageDataOffset & 0x2) {
		imageDataOffset += 2;
	}
	hdr->id[0] = 'B';
	hdr->id[1] = 'M';
	/* total file size */
	hdr->size = imageDataOffset + dib->imageSize;
	hdr->reserved1 = 0;
	hdr->reserved2 = 0;
	hdr->imageDataOffset = imageDataOffset; // hmm
}

int16_t bmp_widthInBytes(int16_t width){
	return  ((width+31) & 0xffe0) >> 3;
}

void bmp_initialize_dib(int16_t width, int16_t height, BITMAPINFOHEADER* dib) {
  // initialize headers
	dib->size = 40;
	dib->width = width; // in pixels
	dib->height = -height;  // negative means rows are top-to-bottom
	dib->numColorPlanes = 1;
	dib->bpp = 1;
	dib->compressionMethod = 0;
	int widthInBytes =  bmp_widthInBytes(width);
	dib->imageSize = widthInBytes * height; // round up, 32-bit boundary
	dib->horizontalResolution = 96;
	dib->verticalResolution = 96;
	dib->numColors = 2; // 2^n
	dib->numImportantColors = 2;
	
	fprintf(stderr, "DIB imageSize is %d or %d * %d\n", dib->imageSize, widthInBytes, height);	
}

void bmp_initialize_color(BITMAPCOLORTABLE* color) {
	color->black = 0x00000000;
	color->white = 0x00ffffff;
}

/*
*  The TMS34010 can address bits. The least significant bit of a word is the leftmost
*  pixel. We bit-reverse each 16-bit word in the bitmap.
*/

/**
* Bit reversal routine
*  public domain
*  from:
*   http://graphics.stanford.edu/~seander/bithacks.html
**/

static const unsigned char BitReverseTable256[] = 
{
  0x00, 0x80, 0x40, 0xC0, 0x20, 0xA0, 0x60, 0xE0, 0x10, 0x90, 0x50, 0xD0, 0x30, 0xB0, 0x70, 0xF0, 
  0x08, 0x88, 0x48, 0xC8, 0x28, 0xA8, 0x68, 0xE8, 0x18, 0x98, 0x58, 0xD8, 0x38, 0xB8, 0x78, 0xF8, 
  0x04, 0x84, 0x44, 0xC4, 0x24, 0xA4, 0x64, 0xE4, 0x14, 0x94, 0x54, 0xD4, 0x34, 0xB4, 0x74, 0xF4, 
  0x0C, 0x8C, 0x4C, 0xCC, 0x2C, 0xAC, 0x6C, 0xEC, 0x1C, 0x9C, 0x5C, 0xDC, 0x3C, 0xBC, 0x7C, 0xFC, 
  0x02, 0x82, 0x42, 0xC2, 0x22, 0xA2, 0x62, 0xE2, 0x12, 0x92, 0x52, 0xD2, 0x32, 0xB2, 0x72, 0xF2, 
  0x0A, 0x8A, 0x4A, 0xCA, 0x2A, 0xAA, 0x6A, 0xEA, 0x1A, 0x9A, 0x5A, 0xDA, 0x3A, 0xBA, 0x7A, 0xFA,
  0x06, 0x86, 0x46, 0xC6, 0x26, 0xA6, 0x66, 0xE6, 0x16, 0x96, 0x56, 0xD6, 0x36, 0xB6, 0x76, 0xF6, 
  0x0E, 0x8E, 0x4E, 0xCE, 0x2E, 0xAE, 0x6E, 0xEE, 0x1E, 0x9E, 0x5E, 0xDE, 0x3E, 0xBE, 0x7E, 0xFE,
  0x01, 0x81, 0x41, 0xC1, 0x21, 0xA1, 0x61, 0xE1, 0x11, 0x91, 0x51, 0xD1, 0x31, 0xB1, 0x71, 0xF1,
  0x09, 0x89, 0x49, 0xC9, 0x29, 0xA9, 0x69, 0xE9, 0x19, 0x99, 0x59, 0xD9, 0x39, 0xB9, 0x79, 0xF9, 
  0x05, 0x85, 0x45, 0xC5, 0x25, 0xA5, 0x65, 0xE5, 0x15, 0x95, 0x55, 0xD5, 0x35, 0xB5, 0x75, 0xF5,
  0x0D, 0x8D, 0x4D, 0xCD, 0x2D, 0xAD, 0x6D, 0xED, 0x1D, 0x9D, 0x5D, 0xDD, 0x3D, 0xBD, 0x7D, 0xFD,
  0x03, 0x83, 0x43, 0xC3, 0x23, 0xA3, 0x63, 0xE3, 0x13, 0x93, 0x53, 0xD3, 0x33, 0xB3, 0x73, 0xF3, 
  0x0B, 0x8B, 0x4B, 0xCB, 0x2B, 0xAB, 0x6B, 0xEB, 0x1B, 0x9B, 0x5B, 0xDB, 0x3B, 0xBB, 0x7B, 0xFB,
  0x07, 0x87, 0x47, 0xC7, 0x27, 0xA7, 0x67, 0xE7, 0x17, 0x97, 0x57, 0xD7, 0x37, 0xB7, 0x77, 0xF7, 
  0x0F, 0x8F, 0x4F, 0xCF, 0x2F, 0xAF, 0x6F, 0xEF, 0x1F, 0x9F, 0x5F, 0xDF, 0x3F, 0xBF, 0x7F, 0xFF
};

// the 16-bit word is replaced (bytes are swapped and reversed)
uint16_t bitwise_reverse_16(uint16_t v) {
	return (BitReverseTable256[v & 0xff] << 8) | 
    (BitReverseTable256[(v >> 8) & 0xff]);
}


// bytes are reversed in-place (not swapped)
uint16_t bitwise_reverse_2x8(uint16_t v) {
	return (BitReverseTable256[v & 0xff]) | 
    (BitReverseTable256[(v >> 8) & 0xff] << 8);
}

uint8_t bitwise_reverse(uint8_t v) {
	return BitReverseTable256[v];
}

// reverse the bytes before writing them to the file
size_t write_row_reverse(uint8_t* patn, size_t stride, FILE* ofp) {
	uint8_t buf[16384];
	for (int i=0; i<stride; ++i) {
		buf[i] = bitwise_reverse(patn[i]);
	}
	return fwrite(buf, stride, 1, ofp);

}

void bmp_write_file(BITMAPFILEHEADER* hdr, 
										BITMAPINFOHEADER* dib,
										BITMAPCOLORTABLE* color,
										uint8_t*  patnTbl, 
										size_t stride, 
										int16_t rows, 
										FILE* ofp) {

	fprintf(stderr, "BITMAPFILEHEADER size is %ld\n", sizeof(BITMAPFILEHEADER));
	fprintf(stderr, "BITMAPINFOHEADER size is %ld\n", sizeof(BITMAPINFOHEADER));
	if (sizeof(BITMAPINFOHEADER) != dib->size) {
		fprintf(stderr, "BITMAPINFOHEADER is not compiled right.\n");
		exit(1);
	}
	fprintf(stderr, "File size is declared as %d\n", hdr->size);
	fprintf(stderr, "\npatnTbl stride is %ld bytes.\n", stride);
	fprintf(stderr, "imageDataOffset is %d\n", hdr->imageDataOffset);
	size_t n = 0;

	/* Headers */
	n += fwrite(hdr, sizeof(BITMAPFILEHEADER), 1, ofp);
	n += fwrite(dib, sizeof(BITMAPINFOHEADER), 1, ofp);

	if (n != 2) {
		fprintf(stderr, "Error writing header to output file: %s\n", strerror(errno));
		exit(1);
	}
	
	/* Color table */
	n = fwrite(color, sizeof(BITMAPCOLORTABLE), 1, ofp);
	if (n == 0) {
		fprintf(stderr, "Error writing color table to output file: %s\n", strerror(errno));
		exit(1);
	}
	
	/* Padding */
	n = paddingToImageData();
	while(n) {
		putc(0, ofp);
		--n;
	}
	/* Pixel array, starting on 4 byte boundary, and each row padded, to 4 byte boundary */
	uint8_t* ptr = patnTbl;
	n = 0;
	for (int16_t i = 0; i<rows; ++i) {
		// do necessary bit-reversal and write it to output
		n = write_row_reverse((uint8_t*)ptr, stride, ofp);
		if (n == 0) {
			fprintf(stderr, "Error writing to output file after %d rows: %s\n", rows-1-i, strerror(errno));
			exit(1);
		}
		// pad to 32-bit DWORD
		if (stride & 0x2) {
			putc(0, ofp);
			putc(0, ofp);
		}
		ptr += stride;
	}
	
	fprintf(stderr, "Wrote %d pattern table rows to output file.\n", rows);
}	


void bmp_write_84(FONT84* font, FILE* ofp) {
	BITMAPFILEHEADER hdr;
	BITMAPINFOHEADER dib;
	
	bmp_initialize_dib(font->rowpitch, font->charhigh, &dib);
	bmp_initialize_hdr(&dib, &hdr);
	
	// must align rows on 32-bit boundaries (they come on 16-bit boundaries)
	uint8_t* patnTbl = (void*)font + bytes(font->oPatnTbl); // pattern table
	size_t   stride = bytes(font->rowpitch);
	
  BITMAPCOLORTABLE color;
	bmp_initialize_color(&color);
	
	bmp_write_file(&hdr, &dib, &color, patnTbl, stride, font->charhigh, ofp);
}


void bmp_write_90(FONT90* font, FILE* ofp) {
	BITMAPFILEHEADER hdr;
	BITMAPINFOHEADER dib;
	
  // initialize headers
	
	bmp_initialize_dib(font->rowwords << 4, font->charhigh, &dib);
	bmp_initialize_hdr(&dib, &hdr);

	uint8_t*  patnTbl = (void*)font + sizeof(FONT90);
	size_t    stride  = font->rowwords << 1;

  BITMAPCOLORTABLE color;
	bmp_initialize_color(&color);
	
	bmp_write_file(&hdr, &dib, &color, patnTbl, stride, font->charhigh, ofp);

}

void bmp_write(FONT* font, FILE* rfp) {
	uint16_t magic = font->magic;

	switch(magic) {
		case MAGIC90:
			bmp_write_90(&font->font90, rfp);
			break;
		case MAGIC84:
			bmp_write_84(&font->font84, rfp);
			break;
		default:
			fprintf(stderr, "Unknown font magic number 0x%x\n", magic);
			break;
	}
}


/**
*   Text Drawing Functions
*
*   Work in progress
**/

int max(int a, int b) {
	return a>b ? a : b;
}

/**
**   A canvas to draw onto
**/
void initialize_bitmap(BITMAPINFOHEADER* dib, BITMAP* bitmap) {
	// chosen dib size is bitmap size
	bitmap->width  = dib->width;
	bitmap->height = dib->height;
	bitmap->stride = bmp_widthInBytes(dib->width); 	// allocate on a BMP boundary 32 bits
	size_t len = bitmap->height * bitmap->stride;
	bitmap->patnTbl   = malloc(len);
	memset(bitmap->patnTbl, 0, len);
}

/** 
	draw a glyph from the font to x,y in the bitmap
	returns width to add to x
**/
int16_t draw_glyph(
	const unsigned char c, 
  int16_t x,
 	int16_t y,
	FONTINFO* info, 
	BITMAP* bitmap) {
	int index = 0;
	// check if it should go to dflt
	if (0) {
		if (info->deflt == 0) {
			// there is no default character; do nothing.
			return 0;
		}
		index = info->deflt - info->first;
	} else {
		index = c - info->first;
	}
	
	// get offset and width
	// if info->charwide!=0 then it is also a fixed width font

	// location in font bitmap
	size_t locn  = info->locTbl[index];
	size_t width = info->locTbl[index+1] - locn;
	if (width == 0) {
		index = info->deflt;
		locn   = info->locTbl[index];
		width = info->locTbl[index+1] - locn;
	}

	// width will be duplicated in owTbl too?
	uint16_t offset = info->owTbl[index] >> 8; // MSByte is the offset
	// character offset: can tuck this character's descender under the previous char
	x = max(x-offset, 0);

	/**
	**   make this a subroutine, copy_glyph( pointers, stride )
	**/


	// Algorithm ideas:
	//
	// Clear a 16-bit register
	// Get a byte from patn into the MSB.
	// Set the number of readyBits to 8.
	// If needed, mask off left bits, subtract from .
	// If needed, mask off right bits.
	// Shift to align the src x with the dest x
	// (this may leave bits in the LSB)
	// OR the byte with dest ptr
	
	// optimization:
	// get two bytes of src into the shift register. mask.
	// if there is a left shift, consider both bytes retired.
	
	// simpler overall:
	// get all the bytes, mask the first and last
	// shift them all to align with dst (this might leave a, or go past edge?)
	// the situation might be: bytes must move left, or right
	//
	// OR them to dst

	// hmm
	// get one byte, mask it, MSB of a 16 bit register
	// urgh, now it is back to the 16 bit register
	// its ok to shift left , the only danger is losing 0s right?
	// shift left 8 bits
	// get next byte, OR it
	
	// process 1 src byte to completion, before fetching next byte.
		
		
	
	// masks indexed by x mod 8	
	char masks[8] = { 0xff, 0x7f, 0x3f, 0x1f, 0x0f, 0x07, 0x03, 0x01 };

	int16_t totalWidth = 0; // accumulated widths
	uint16_t cbuffer; // 16 bit work buffer
	for (int row = 0; row < info->charhigh; ++row) {
		// patn:           xxx11111 11111111 1xxxxxxx
		// mask:           00011111 11111111 10000000
		// bitmap:  ........ ........ ........ ........
		// start:             x
		
		// work from byte containing left edge, toward right
		
		cbuffer = 0;
//		for (int col = 0; col < width; ++col) {
		// Get srcPtr into patn
		uint8_t* srcPtr = info->patnTbl + (y * info->stride) + locn;
		
		// Get dstPtr into bitmap
		uint8_t* dstPtr = bitmap->patnTbl + (y+row) * info->stride + x;
		
	/**
	**    Not implemented
	**/
		
		
	}
	
	return totalWidth;
}

int text_width(const unsigned char* text) {
	const unsigned char* str = text;
	int width = 0;
	int last = 0;
	while(*str) {
		// if new row, set last
		// last = max(last, width);
		// width = 0;
		// add from tbl
		width += 0;
		++str;
	}
	return max(last, width);
}



/** Text string formatter **/
void text_out(unsigned char* const text, FONT* font, FILE* ofp) {
	BITMAPFILEHEADER hdr;
	BITMAPINFOHEADER dib;
  BITMAPCOLORTABLE color;
	BITMAP	 bitmap;
	FONTINFO info;
	
	get_font_info(font, &info);

	int width  = text_width(text);
	int height = info.charhigh;

	bmp_initialize_dib(width, height, &dib);
	bmp_initialize_hdr(&dib, &hdr);
	bmp_initialize_color(&color);
	
	initialize_bitmap(&dib, &bitmap);
	
	unsigned char* str = text;
	int x = 0;
	int y = 0;
	int right = 0;
	while(*str) {
		right = draw_glyph(*str, x, y, &info, &bitmap);
		x += right;
		// if new row, x=0, y+= ascent+descent+leading
		++str;
	}
			
	bmp_write_file(&hdr, &dib, &color, bitmap.patnTbl, bitmap.stride, bitmap.height, ofp);
}

/**
  *  File Processing Functions
**/


unsigned char* read_file(FILE *ifp, size_t len) {
	unsigned char *buf = (unsigned char*)malloc(len+1);
	if (0 == fread(buf, len, 1, ifp)) {
		free(buf);
		buf = NULL;
	} else {
		buf[len] = 0; // make sure it is terminated
	}
	return buf;
}

void process_file(FILE* ifp, FILE* ofp, FILE* rfp, const char* input_filename) {
  struct stat stats;
	fstat(fileno(ifp), &stats);
	int len = stats.st_size;
	unsigned char *buf = read_file(ifp, len);
	if (!buf) {
		fprintf(stderr, "Error reading input file %s: %s\n", input_filename, strerror(errno));
		exit(1);
	}    
	
	/*  */
		
	fprintf(rfp, "*\n*      %s\n*\n", input_filename);
	
	process_binary(buf, len, ofp, rfp);

	if (rfp) {

	}
	
	if (buf) {
		free(buf);
	}

}


/* Assuming the buffer is binary data.
   len is the number of binary bytes.
*/

void process_binary(unsigned char* buf, int len, FILE* ofp, FILE* rfp) {
	
  fprintf(stderr, "Processing %d bytes.\n", len);

	/* read font header */
	
	FONT* font = (FONT*)buf;
	
	print_font_header(font, rfp);
	bmp_write(font, ofp);
}



/** 
 * take_bits
 *  Return n bits out of the bitmap. Keep pbit across calls
 *  to this function.
 *  Return bits are right-justified in an int.
 *  (so the max n depends on sizeof(int).)
 *  Not especially efficient, but.
 *  
**/

int take_bits(unsigned char** ptr, int n, int *pbit) {
  unsigned char masks[8] = { 0x1, 0x2, 0x4, 0x8, 0x10, 0x20, 0x40, 0x80 };

  int x = 0;

  int i=0;
  do {
    /* loop through available bits */
		for (; i<n && *pbit<8; (*pbit)++, i++) {
		  // get one bit
			int bit = (**ptr) & masks[*pbit] ? 1 : 0;
			x = (x<<1) + bit;
		}

		/* if byte is exhausted, replenish bits */
		if (*pbit == 8) {
			++(*ptr);
			*pbit = 0;
		}

	} while(i<n); // continue in new byte

	return x;
}


int main(int argc, char **argv) {
    if (argc<3) {
        fprintf(stderr, "Usage: %s inputfile-lpc-csv outputfile-src [outputfile-decoded]\n", argv[0]);
        exit(1);
    }
    
    const char* input_filename = argv[1];
    FILE *ifp = fopen(input_filename, "r");
    if (ifp == NULL) {
        fprintf(stderr, "%s: can't read input %s: %s\n", argv[0], input_filename, strerror(errno));
        exit(1);
    }

    FILE *ofp = fopen(argv[2], "w");
    if (ofp == NULL) {
        fprintf(stderr, "%s: can't write output %s: %s\n", argv[0], argv[2], strerror(errno));
        exit(1);
    }

		/* Optional output file of decode report. '-' means stdout. */
    FILE *rfp = NULL;
    if (argc > 3) {
    	if (*argv[3] == '-') {
    		rfp = stdout;
    	} else {
				rfp = fopen(argv[3], "w");
				if (rfp == NULL) {
					fprintf(stderr, "%s: can't write report %s: %s\n", argv[0], argv[3], strerror(errno));
					exit(1);
				}
			}
    }

		/* Get input file size, then load it all. */
    struct stat stats;    
    fstat(fileno(ifp), &stats);
    
    /* is it a directory? */
    if (stats.st_mode & S_IFDIR) {
    	fprintf(stderr, "%s: Processing directory--not yet implemented\n", argv[0]);
    	exit(1);
    }
    
    process_file(ifp, ofp, rfp, input_filename);

    fclose(ifp);
		fclose(ofp);

		if (rfp && rfp != stdout) {
	    fclose(rfp);
	  }
}

/**

References:

TMS340 Graphics Library User’s Guide.
https://www.ti.com/lit/ug/spvu027/spvu027.pdf

References listed in there, which I want to read:

TMS34010 and TMS34020 Graphics System Processors

	Asal, Mike, Graham Short, Tom Preston, Derek Roskell, and Karl Guttag. “The Texas Instruments 34010 Graphics System Processor.” IEEE
	Computer Graphics & Applications, vol. 6, no. 10 (October 1986),
	pages 24–39.

	Guttag, Karl, Jerry Van Aken, and Mike Asal. “Requirements for a VLSI
	Graphics Processor.” IEEE Computer Graphics & Applications, vol. 6,
	no. 1 (January 1986), pages 32–47.

	Killebrew, Carrell R., Jr., “The TMS34010 Graphics System Processor.”
	Byte, vol. 11, no. 12 (December 1986), pages 193–204.

	Peterson, Ron, Carrell R. Killebrew, Jr., Tom Albers and Karl Guttag.
	“Taking the Wraps off the 34020. ” Byte, vol. 11, no. 9 (September
	1986), pages 257–272.

Video Memories and Displays

	Pinkham, Ray, Mark Novak, and Karl Guttag. “Video RAM Excels at
	Fast Graphics.” Electronic Design, vol. 31, no. 17 (August 1983), pages
	161–182.

	Whitton, Mary C. “Memory Design for Raster Graphics Displays.” IEEE
	Computer Graphics & Applications, vol. 4, no. 3 (March 1984), pages
	48–65.

PNG file I/O
	On github: look up stv

	They aim to create self-contained headers which implement these functions.
	
Also see kguttag.com

**/
